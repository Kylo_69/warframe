import requests
import json
import os
from dotenv import load_dotenv

# Load variables from .env file
load_dotenv()

# Get the Discord token from environment variables
discord_token = os.getenv("discord_token")


DISCORD_API = "https://discord.com/api"
DISCORD_CHANNEL_ID = "346784293269536778"
LFM_API = "https://api2.lowfuelmotorsport.com/api"

# you figure discord token out
def send_discord_message(content, discord_channel=DISCORD_CHANNEL_ID, token: str=discord_token):
    url = f"{DISCORD_API}/v9/channels/{discord_channel}/messages"
    headers = {
        "Authorization": token,
        "Content-Type": "application/json"
    }

    data = {"content": str(content)}

    response = requests.post(url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        response_json = response.json()
        message_id = response_json.get("id")
        print("Request successful! Message ID:", message_id)
        return message_id
    else:
        print("Request failed with status code:", response.status_code, response.text)

# you figure discord token out
def send_crosspost(message_id, discord_channel=DISCORD_CHANNEL_ID, token: str=discord_token):
    url = f"{DISCORD_API}/v9/channels/{discord_channel}/messages/{message_id}/crosspost"
    print(url)
    headers = {
        "Authorization": token,
        "Content-Type": "application/json"
    }

    response = requests.post(url, headers=headers)

    if response.status_code == 200:
        print("Request successful!")
    else:
        print("Request failed with status code:", response.status_code, response.text)
