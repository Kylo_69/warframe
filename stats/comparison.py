import os
import csv
from datetime import datetime, timedelta
from pathlib import Path
import send_discord_message
from dateutil.relativedelta import relativedelta

def seconds_to_readable(seconds):
    td = timedelta(seconds=seconds)
    hours, remainder = divmod(td.total_seconds(), 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{int(hours)}h {int(minutes)}m {seconds:.3f}s"

def get_session_folders():
    base_dir = "weapon_stats"
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)
        print(f"Created {base_dir} directory. Add timestamped CSV files first.")
        exit()

    sessions = sorted([d for d in os.listdir(base_dir) if os.path.isdir(os.path.join(base_dir, d))])
    if not sessions:
        print(f"No session folders in {base_dir}! Name folders with timestamps like YYYYMMDD.")
        exit()

    print("\nAvailable sessions (chronological order):")
    for i, session in enumerate(sessions, 1):
        print(f"{i}. {session}")
    
    selected = sessions[int(input("\nEnter session number: "))-1]
    return os.path.join(base_dir, selected)


def get_csv_files(session_path):
    files = sorted([f for f in os.listdir(session_path) if f.endswith('.csv')])
    if len(files) < 2:
        print("Need at least 2 CSV files for comparison!")
        exit()
    
    print("\nAvailable snapshots (chronological order):")
    for i, f in enumerate(files, 1):
        print(f"{i}. {f}")
    return files, session_path


def read_csv_file(full_path):
    data = {}
    with open(full_path, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            try:
                data[row['Internal Name']] = {
                    'Weapon': (row['Weapon']),
                    'Equip Time': float(row['Equip Time']),
                    'Kills': int(row['Kills']),
                    'Assists': int(row['Assists']),
                    'Headshots': int(row['Headshots']),
                    'Hits': int(row['Hits']),
                    'Fired': int(row['Fired'])
                }
            except KeyError:
                print(f"Invalid CSV format in {os.path.basename(full_path)}")
                exit()
    return data

def compare_files(newer_data, older_data):
    comparison = []
    #for weapon in newer_data.keys():
    for weapon in newer_data.keys():
        new = newer_data[weapon]
        old = older_data.get(weapon, {'Equip Time': 0, 'Kills': 0, 'Assists': 0, 'Headshots': 0, 'Hits': 0, 'Fired': 0})

        #import time
        # if old['Equip Time'] != new['Equip Time']:
        #     print(new)
        #     print(old)
        #     time.sleep(10)

        deltas = {
            'internal': weapon,
            'name': new['Weapon'],
            'equip': new['Equip Time'] - old['Equip Time'],
            'kills': new['Kills'] - old['Kills'],
            'assists': new['Assists'] - old['Assists'],
            'headshots': new['Headshots'] - old['Headshots'],
            'hits': new['Hits'] - old['Hits'],
            'fired': new['Fired'] - old['Fired']
        }
        
        if deltas['equip'] > 0 or deltas['kills'] > 0 or deltas['assists'] > 0:
            comparison.append({
                'Internal': weapon,
                'Weapon': deltas['name'],
                'Equip Delta': deltas['equip'],
                'Readable Delta': seconds_to_readable(deltas['equip']),
                'Kills Delta': deltas['kills'],
                'Assists Delta': deltas['assists'],
                'Headshots Delta': deltas['headshots'],
                'Hits Delta': deltas['hits'],
                'Fired Delta': deltas['fired']
            })
    
    return sorted(comparison, key=lambda x: x['Equip Delta'], reverse=True)


def find_files_modified_around_x_days_ago(directory, weekly) -> str:
    # Get the current time
    now = datetime.now()

    # Calculate the time 7 days ago
    if weekly:
        seven_days_ago = now - timedelta(days=7)
        time_range = timedelta(hours=12)
        lower_bound = seven_days_ago - time_range
        upper_bound = seven_days_ago + time_range
    else:
        seven_days_ago = now - relativedelta(months=1)
        time_range = timedelta(hours=72)
        lower_bound = seven_days_ago - time_range
        upper_bound = seven_days_ago + time_range

    # Define a time range (e.g., +/- 3 hours around 7 days ago)

    print(lower_bound, upper_bound)

    # Iterate through files in the directory
    for filename in os.listdir(directory):
        file_path = os.path.join(directory, filename)
        filename = filename[:-10]

        #print(filename)
        timestamp = datetime.strptime(filename, "%Y-%m-%d_%H-%M-%S.%f")

        #print(lower_bound, timestamp, upper_bound)

        # Check if the modification time is within the range
        if lower_bound <= timestamp <= upper_bound:
            return file_path
            

def send_in_discord(weekly=True):
    users = ["Simmml", "Kylo", "Sairento", "phil199"]
    #users = ["Kylo"]

    for user in users:
        session_path = os.path.join("weapon_stats", user)
        files, session_path = get_csv_files(session_path)

        old_file = find_files_modified_around_x_days_ago(session_path, weekly)

        result = ""
        # File selection
        file1_idx = 0
        file2_idx = len(files)-1

        #file1 = os.path.join(session_path, files[file1_idx])
        file1 = old_file
        file2 = os.path.join(session_path, files[file2_idx])

        print(file1, file2)
        
        # Parse into datetime object
        dt_1 = datetime.strptime(Path(file1).stem.split('+')[0], "%Y-%m-%d_%H-%M-%S.%f")
        dt_2 = datetime.strptime(Path(file2).stem.split('+')[0], "%Y-%m-%d_%H-%M-%S.%f")

        print(file1, file2)

        print(f"\nComparing:\nFILE1: {os.path.basename(file1)}\nFILE2: {os.path.basename(file2)}")
        
        data1 = read_csv_file(file1)
        data2 = read_csv_file(file2)
        
        results = compare_files(data2, data1) if file2_idx > file1_idx else compare_files(data1, data2)
        
        # Sorted by EQUIP DELTA
        result = f"**Top 10 Item by Usage for {user}**\n-# From {dt_1} to {dt_2}\n"
        result += "```\n"
        result += (f"{'Weapon':<50}\t{'Time Used':<16}\t{'Kills':<10}\t{'Assists':<10}\n")
        for row in results[:10]:
            result += (f"{row['Weapon']:<50}\t{row['Readable Delta']:<16}\t"
                f"{row['Kills Delta']:<10}\t{row['Assists Delta']:<10}\n")
        result += "```"
            
        print(result)
        send_discord_message.send_discord_message(result)
        
        # Sorted by KILLS DELTA
        results = sorted(results, key=lambda x: x['Kills Delta'], reverse=True)
        result = f"**Top 10 Item by Kills for {user}**\n-# From {dt_1} to {dt_2}\n"
        result += "```\n"
        result += (f"{'Weapon':<50}\t{'Time Used':<16}\t{'Kills':<10}\t{'Assists':<10}\n")
        for row in results[:10]:
            result += (f"{row['Weapon']:<50}\t{row['Readable Delta']:<16}\t"
                f"{row['Kills Delta']:<10}\t{row['Assists Delta']:<10}\n")
        result += "```"
            
        print(result)
        send_discord_message.send_discord_message(result)

        result = ""

        results = sorted(results, key=lambda x: x['Equip Delta'], reverse=True)
        result += f"Most Used Item: {results[0]['Weapon']}: {results[0]['Readable Delta']}\n"
        
        results = sorted(results, key=lambda x: x['Kills Delta'], reverse=True)
        result += f"Most Kills Item: {results[0]['Weapon']}: {results[0]['Kills Delta']}\n"
        
        results = sorted(results, key=lambda x: x['Assists Delta'], reverse=True)
        result += f"Most Assists Item: {results[0]['Weapon']}: {results[0]['Assists Delta']}\n"
        
        results = sorted(results, key=lambda x: x['Headshots Delta'], reverse=True)
        result += f"Most Headshot with: {results[0]['Weapon']}: {results[0]['Headshots Delta']}\n"
        
        results = sorted(results, key=lambda x: x['Hits Delta'], reverse=True)
        result += f"Most Hits with: {results[0]['Weapon']}: {results[0]['Hits Delta']}\n"
        
        results = sorted(results, key=lambda x: x['Fired Delta'], reverse=True)
        result += f"Most Fired Item: {results[0]['Weapon']}: {results[0]['Fired Delta']}\n"
        
        print(result)
        send_discord_message.send_discord_message(result)

        import time
        time.sleep(300)


def main():
    session_path = get_session_folders()
    files, session_path = get_csv_files(session_path)
    
    # File selection
    file1_idx = int(input("\nEnter first file number: "))-1
    file2_idx = int(input("Enter second file number: "))-1
    
    file1 = os.path.join(session_path, files[file1_idx])
    file2 = os.path.join(session_path, files[file2_idx])
    
    print(f"\nComparing:\nFILE1: {os.path.basename(file1)}\nFILE2: {os.path.basename(file2)}")
    
    data1 = read_csv_file(file1)
    data2 = read_csv_file(file2)
    
    results = compare_files(data2, data1) if file2_idx > file1_idx else compare_files(data1, data2)
    #results = [item for item in results if item['Weapon'] != "Unknown Item"]
    
    # Terminal output
    print("\nTop 10 Weapons by Usage:")
    print(f"{'Weapon':<20} | {'Time Used':<16} | {'Kills':<10} | {'Assists':<10}")
    print("-" * 58)
    for row in results[:10]:
        print(f"{row['Weapon']:<20} | {row['Readable Delta']:<16} | "
              f"{row['Kills Delta']:<10} | {row['Assists Delta']:<10}")
        
    
    results = sorted(results, key=lambda x: x['Kills Delta'], reverse=True)
    # Terminal output
    print("\nTop 10 Weapons by Kills:")
    print(f"{'Weapon':<20} | {'Time Used':<16} | {'Kills':<10} | {'Assists':<10}")
    print("-" * 58)
    for row in results[:10]:
        print(f"{row['Weapon']:<20} | {row['Readable Delta']:<16} | "
              f"{row['Kills Delta']:<10} | {row['Assists Delta']:<10}")

    # Safe save handling
    save_path = input("\nEnter filename to save comparison data (or press Enter to skip): ").strip()
    if save_path:
        with open(save_path, 'w', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=results[0].keys())
            writer.writeheader()
            writer.writerows(results)
        print(f"Saved results to: {save_path}")
    else:
        print("Comparison complete - no data saved")

if __name__ == "__main__":
    main()