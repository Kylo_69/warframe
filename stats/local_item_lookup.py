import csv

def lookup(search_string: str):

    matches = []
    filename = 'item_names.csv'
    
    try:
        with open(filename, 'r', newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if len(row) >= 2 and row[0] == search_string:
                    matches.append(row[1])
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        return
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        return

    if matches:
        #print("Matching results:")
        for result in matches:
            #print(result)
            return result
    else:
        return "Unknown Item"
        #print(f"No matches found for '{search_string}' in column 1.")
