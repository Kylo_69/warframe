import csv
import list_items
import edit_listing

def read_csv(file_path):
    items = {}
    with open(file_path, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            item_name = row['Item Name'].strip()
            quantity = int(row['Quantity'])
            items[item_name] = quantity
    return items

def write_csv(file_path, items):
    with open(file_path, 'w', newline='') as csvfile:
        fieldnames = ['Item Name', 'Quantity']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for item_name, quantity in items.items():
            writer.writerow({'Item Name': item_name, 'Quantity': quantity})

def main() -> str:
    price = 0
    user_input = input("Enter an item name: ").strip().title()

    try:
        price = list_items.get_minimum_price(user_input)
        print(f"Item Price: {price}")
    except:
        print("Item has no value.")

    if price >= 10:
        #list_items.list_item(user_input, price, 1)
        #print(f"Listed {user_input} for {price}")
        edit_listing.main(user_input, 1, price)
    else:
        csv_file_path = 'old_items.csv'
        print(f"Added {user_input} to old_items.csv")
        items = read_csv(csv_file_path)    
        if user_input in items:
            items[user_input] += 1
        else:
            items[user_input] = 1

        write_csv(csv_file_path, items)
        print(f"Updated quantity for {user_input}: {items[user_input]}")


    return (user_input, price)

if __name__ == "__main__":
    main()
