import requests
import json
import csv
import time
from datetime import datetime, timedelta
import logging
import sys
import pandas as pd
import os
from dotenv import load_dotenv

import list_items

# Load environment variables from .env file
load_dotenv()

# Access the variables
email = os.getenv("EMAIL")
password = os.getenv("PASSWORD")
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logging.info("Loaded email and password.")

WFM_API = "https://api.warframe.market/v1"
TOO_LOW_PRICE = 10

def login(
    user_email: str, user_password: str, platform: str = "pc", language: str = "en"
):
    """
    Used for logging into warframe.market via the API.
    Returns (User_Name, JWT_Token) on success,
    or returns (None, None) if unsuccessful.
    """
    headers = {
        "Content-Type": "application/json; utf-8",
        "Accept": "application/json",
        "Authorization": "JWT",
        "platform": platform,
        "language": language,
    }
    content = {"email": user_email, "password": user_password, "auth_type": "header"}
    response = requests.post(f"{WFM_API}/auth/signin", data=json.dumps(content), headers=headers)
    if response.status_code != 200:
        return None, None
    return (response.json()["payload"]["user"]["ingame_name"], response.headers["Authorization"])

user, access = login(email, password)
# put login here

headers = {
        "Content-Type": "application/json; utf-8",
        "Accept": "application/json",
        "auth_type": "header",
        "platform": "pc",
        "language": "en",
        "Authorization": access,
        'User-Agent': 'Kylo\'s Auto Lister',
}


def get_order_id_and_quantity(item, username = user):

    search_item = list_items.get_item_name(item)
    # Define the order endpoint
    order_url = WFM_API + f"/profile/{username}/orders"

    # Send a POST request to create an order
    order_response = requests.get(order_url, headers=headers)

    data = order_response.text
    # Load the JSON data
    sell_order_data = json.loads(data)

    sell_orders = sell_order_data['payload']['sell_orders']
    
    print(search_item)
    for order in sell_orders:
        if search_item == order['item']['url_name']:
            return (order['id'], order['quantity'])
        
def edit_listing(order_id, price, quantity):
        # New order details
    data = {
    "id": order_id,
    "platinum": int(price),
    "quantity": int(quantity),
    "visible": True,
    }

    order_url = WFM_API + f"/profile/orders/{order_id}"

    # Make POST request to edit order
    response = requests.put(order_url, headers=headers, json=data)
    print(response)

    # Check for success
    if response.status_code == 200:
        print("Order edited successfully!")
    else:
        print("Error editing order:", response.text)

def main(item, price = None):
    if price == None:
        price = list_items.get_minimum_price(item)
        
    id, quantity = get_order_id_and_quantity(item)
    time.sleep(1)
    edit_listing(id, price, quantity+1)
