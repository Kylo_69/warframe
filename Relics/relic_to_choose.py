import pandas as pd
import sys
import os

def get_relic(input):
    # Load the relic data from the first CSV file
    relics_df = pd.read_csv('relics.csv')

    # Load the adjustment data from the second CSV file
    adjustments_df = pd.read_csv(f'{input}.csv', dtype={"amount": "float"})

    adjustments_df = adjustments_df[adjustments_df['Relic Name'].str.contains(input, case=False, na=False)]


    # Calculate the products for all relics and add them to a new column
    adjustments_df['Product'] = adjustments_df.apply(
        lambda row: row['amount'] * float(relics_df[relics_df['Relic Name'] == row['Relic Name']]['sum Adjusted'].values[0]) if not relics_df[relics_df['Relic Name'] == row['Relic Name']].empty else 0, axis=1)

    # Filter relics with an amount greater than 0
    filtered_adjustments_df = adjustments_df[adjustments_df['amount'] > 0]

    # Sort the DataFrame by the 'Product' column in ascending order
    sorted_df = filtered_adjustments_df.sort_values(by='Product', ascending=False)

    # Get the relic with the lowest product (top one)
    top_relic = sorted_df.iloc[0]
    # Print the top 5 rows of the sorted DataFrame without the row index
    print(sorted_df.head(5).to_string(index=False))

    # Update the second CSV file and reduce the number by 1 for the top relic
    relic_name = top_relic['Relic Name']
    adjustments_df.loc[adjustments_df['Relic Name'] == relic_name, 'amount'] = adjustments_df.loc[adjustments_df['Relic Name'] == relic_name, 'amount'] - 1

    # Save the updated adjustments DataFrame back to the adjustments.csv file
    adjustments_df.to_csv(f'{input}.csv', index=False)

    return relic_name


def combine_adjustments(files):
    combined_df = pd.DataFrame()
    for file in files:
        if os.path.exists(file):  # Check if file exists
            df = pd.read_csv(file, dtype={"amount": "float"})
            combined_df = pd.concat([combined_df, df], ignore_index=True)
    return combined_df

def get_omni_relic():
    # Combine data from lith.csv, meso.csv, neo.csv, and axi.csv
    files = ['lith.csv', 'meso.csv', 'neo.csv', 'axi.csv']
    adjustments_df = combine_adjustments(files)

    # Load the relic data
    relics_df = pd.read_csv('relics.csv')

    # Calculate the products for all relics and add them to a new column
    adjustments_df['Product'] = adjustments_df.apply(
        lambda row: row['amount'] * float(relics_df[relics_df['Relic Name'] == row['Relic Name']]['sum Adjusted'].values[0]) if not relics_df[relics_df['Relic Name'] == row['Relic Name']].empty else 0, axis=1)

    # Filter relics with an amount greater than 0
    filtered_adjustments_df = adjustments_df[adjustments_df['amount'] > 0]

    # Sort the DataFrame by the 'Product' column in ascending order
    sorted_df = filtered_adjustments_df.sort_values(by='Product', ascending=False)

    # Get the relic with the lowest product (top one)
    top_relic = sorted_df.iloc[0]
    # Print the top 5 rows of the sorted DataFrame without the row index
    print(sorted_df.head(5).to_string(index=False))

    # Update the corresponding CSV file and reduce the number by 1 for the top relic
    relic_name = top_relic['Relic Name']
    for file in files:
        if os.path.exists(file):
            df = pd.read_csv(file)
            if relic_name in df['Relic Name'].values:
                df.loc[df['Relic Name'] == relic_name, 'amount'] -= 1
                df.to_csv(file, index=False)
    
    return relic_name


def print_all_relics():
    # Combine data from lith.csv, meso.csv, neo.csv, and axi.csv
    files = ['lith.csv', 'meso.csv', 'neo.csv', 'axi.csv']
    adjustments_df = combine_adjustments(files)

    # Load the relic data
    relics_df = pd.read_csv('relics.csv')

    # Calculate the products for all relics and add them to a new column
    adjustments_df['Product'] = adjustments_df.apply(
        lambda row: row['amount'] * float(relics_df[relics_df['Relic Name'] == row['Relic Name']]['sum Adjusted'].values[0]) if not relics_df[relics_df['Relic Name'] == row['Relic Name']].empty else 0, axis=1)

    # Filter relics with an amount greater than 0
    filtered_adjustments_df = adjustments_df[adjustments_df['amount'] > 0]

    # Sort the DataFrame by the 'Product' column in ascending order
    sorted_df = filtered_adjustments_df.sort_values(by='Product', ascending=False)

    # Get the relic with the lowest product (top one)
    top_relic = sorted_df.iloc[0]
    # Print the top 5 rows of the sorted DataFrame without the row index
    print(sorted_df.to_string(index=False))