relics.py to use while running relics to determine what relic is "best" to use.

0. Create .env file with EMAIL and PASSWORD variable, each in a new line no " for the strings. (email and password for warframe.market, if you dont have it, because you log in via steam etc, make an account with the email
1. Add all your radiant relics to lith.csv, meso.csv, neo.csv and axi.csv.
2. Clear relics.counts.txt (It got my data atm)
3. Run relics.py, select lith/meso/neo/axi for normal fissures or omni for omni fissures.
3.1 omni will combine the lith, meso, neo and axi relics.
4. When you cracked the relic it will ask you for rarity. put in either "rare", "uncommon", or "common". Need to be the exact strings, program wont crash if it's wrong though.
5. It will ask you for your item you selected/received.
6. If item is above or 10p worth on warframe.market it will list it for the minimum price in the last 6 hours times 1.5
7. Click enter to go again.

(You need the relic prices, I will update them from time to time)
