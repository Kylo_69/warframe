import time
import re
import csv
import pandas as pd
import relic_to_choose
import save_items
import find_item
import list_items

def format_elapsed_time(elapsed_time):
    days, remainder = divmod(elapsed_time, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, seconds = divmod(remainder, 60)

    time_str = ""
    if days: 
        time_str += f"{int(days)}d "
    if hours:
        time_str += f"{int(hours)}h "
    if minutes:
        time_str += f"{int(minutes)}min "
    if seconds:
        time_str += f"{int(seconds)}sec "

    return time_str


def append_to_csv(file_path, data):
    # Check if the file exists, if not, create it with headers
    file_exists = False
    try:
        with open(file_path, 'r') as file:
            reader = csv.reader(file)
            if next(reader, None) is not None:  # Check if there is at least one row
                file_exists = True
    except FileNotFoundError:
        pass  # File does not exist

    with open(file_path, 'a', newline='') as file:
        writer = csv.writer(file)

        # Write headers if the file was just created
        if not file_exists:
            writer.writerow(["relic", "drop", "reward"])

        # Append data
        writer.writerow(data)


# Define drop chances
chances = {
	"common": 0.5,
	"uncommon": 0.4,
	"rare": 0.1
}

# Initialize counts
counts = {
	"common": 0,
	"uncommon": 0,
	"rare": 0
}

with open('relic_counts.txt', 'r') as file:
	text = file.read()

# Use regular expressions to extract the values
matches = re.findall(r'(\d+) \([-+]?\d+\.\d+\)', text)

# Extract the average time per relic
average_time_match = re.search(r'Average time per relic: ([\d.]+) minutes', text)
average_time = float(average_time_match.group(1))

# Convert the extracted values to integers
counts['common'] = int(matches[0])
counts['uncommon'] = int(matches[1])
counts['rare'] = int(matches[2])

# Calculate the sum of common, uncommon, and rare multiplied by the average time per relic
total_time = (counts['common'] + counts['uncommon'] + counts['rare']) * average_time
total_time *= 60
print(total_time)
total_price = 0

while True:
	try:
		
		# Load the adjustment data from the CSV file
		axi = pd.read_csv('axi.csv', dtype={"amount": "float"})
		neo = pd.read_csv('neo.csv', dtype={"amount": "float"})
		meso = pd.read_csv('meso.csv', dtype={"amount": "float"})
		lith = pd.read_csv('lith.csv', dtype={"amount": "float"})

		# Calculate the sum of the 'amount' column
		total_relics = axi['amount'].sum() + meso['amount'].sum() + neo['amount'].sum() + lith['amount'].sum()
		
		input_str = input("Enter relic (lith/meso/neo/axi): ")
		# Start timer
		start_time = time.time()
		relic = None
		if input_str == "omni":
			relic = relic_to_choose.get_omni_relic()
		else:
			relic = relic_to_choose.get_relic(input_str)
			print(f'{int(pd.read_csv(f"{input_str}.csv", dtype={"amount": "float"})["amount"].sum())} Relics of type {input_str.title()}')
		print(relic)

		print(f"Total Radiant Relics: {int(total_relics)}")

		input_str = input("Enter drop (common/uncommon/rare or 'exit'): ")
		if input_str == "exit":
			break
		
		drop_type = input_str.lower()
		drop_type_temp = drop_type

		if drop_type in chances:
			counts[drop_type] += 1
		else:
			print("Invalid drop type")

		#print(time.time() - start_time)
		total_time = time.time() - start_time + total_time
		average_time_per_relic = (total_time / sum(counts.values())) / 60
		time_to_finish = average_time_per_relic*(total_relics)
		time_to_finish_formatted = format_elapsed_time(time_to_finish*60)

		output = ""
		for drop_type, count in counts.items():
			expected_count = chances[drop_type] * sum(counts.values())
			difference = (count - expected_count)
			if difference > 0:
				output += f"{drop_type.capitalize()}: {count} (+{difference:.2f})   "
			else:
				output += f"{drop_type.capitalize()}: {count} ({difference:.2f})   "

		output += f"\nAverage time per relic: {average_time_per_relic:.2f} minutes\n" 
		output += f"Time until done: {time_to_finish_formatted}" 
		with open("relic_counts.txt", "w") as output_file:
			output_file.write(output)
			output_file.flush()  # Flush the buffer to ensure content is written immediately
		item, price = save_items.main()
		total_price += price
		print(total_price)		
		data_to_append = [relic, drop_type_temp, item]
		append_to_csv("relic_runs.csv", data_to_append)
		print(output)
		print(f"Elapsed time: {total_time}")
		input("Press enter to continue.")
		print("__________")

	except KeyboardInterrupt:
		print("\nExiting...")
		break	

output_file.close()