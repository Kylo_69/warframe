# Import requests library
import requests
import json
import csv
import time
from datetime import datetime, timedelta
import logging
import sys
import pandas as pd
import os
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Access the variables
email = os.getenv("EMAIL")
password = os.getenv("PASSWORD")
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logging.info("Loaded email and password.")

WFM_API = "https://api.warframe.market/v1"
TOO_LOW_PRICE = 10

def login(
    user_email: str, user_password: str, platform: str = "pc", language: str = "en"
):
    """
    Used for logging into warframe.market via the API.
    Returns (User_Name, JWT_Token) on success,
    or returns (None, None) if unsuccessful.
    """
    headers = {
        "Content-Type": "application/json; utf-8",
        "Accept": "application/json",
        "Authorization": "JWT",
        "platform": platform,
        "language": language,
    }
    content = {"email": user_email, "password": user_password, "auth_type": "header"}
    response = requests.post(f"{WFM_API}/auth/signin", data=json.dumps(content), headers=headers)
    if response.status_code != 200:
        return None, None
    return (response.json()["payload"]["user"]["ingame_name"], response.headers["Authorization"])

user, access = login(email, password)
# put login here

headers = {
        "Content-Type": "application/json; utf-8",
        "Accept": "application/json",
        "auth_type": "header",
        "platform": "pc",
        "language": "en",
        "Authorization": access,
        'User-Agent': 'Kylo\'s Auto Lister',
}

def get_item_name(item_name):

    # Define the endpoint
    item_url = WFM_API + f"/items"
    
    order_response = requests.get(item_url, headers=headers)

    data = order_response.json()

    for item in data["payload"]["items"]:
        if item["item_name"] == item_name:
            return item["url_name"]
    return None  # Return None if the item is not found

def get_id(item_name):

    # Define the endpoint
    item_url = WFM_API + f"/items"
    
    order_response = requests.get(item_url, headers=headers)

    data = order_response.json()

    for item in data["payload"]["items"]:
        if item["item_name"] == item_name:
            return item["id"]
    return None  # Return None if the item is not found


def list_item(item, price, quantity):

    # Define the item name and price you want to list
    platinum = price
    quantity = quantity
    item_id = get_id(item)
    order_type = "sell"

    # Define the order endpoint
    order_url = WFM_API + f"/profile/orders"

    # Define the payload for order
    json_data = {
        "item": str(item_id),
        "order_type": str(order_type),
        "platinum": int(platinum),
        "quantity": int(quantity),
        "visible": True,
    }

    # Send a POST request to create an order
    order_response = requests.post(order_url, headers=headers, json=json_data)

    # Check if the order request was successful
    if order_response.status_code == 200:
        # logging.info a success message
        logging.info(f"Successfully listed {item} [x{quantity}] for {platinum} platinum on your orders.")
    else:
        # logging.info an error message
        logging.info(f"Failed to list an item. Error: {order_response.text}")

def is_listed(item, username = user):

    search_item = get_item_name(item)
    # Define the order endpoint
    order_url = WFM_API + f"/profile/{username}/orders"

    # Send a POST request to create an order
    order_response = requests.get(order_url, headers=headers)

    data = order_response.text
    # Load the JSON data
    sell_order_data = json.loads(data)

    sell_orders = sell_order_data['payload']['sell_orders']

    # Check if search_item is in any of the 'url_name' values within 'sell_orders'
    found = any(search_item in order['item']['url_name'] for order in sell_orders)
    

    # If found is True, it means the item was found, otherwise, it wasn't.
    if found:
        time.sleep(1)
        return True
    else:
        time.sleep(1)
        return False

def get_minimum_price(search_item):

    item = get_item_name(search_item)

    # Define the order endpoint
    order_url = WFM_API + f"/items/{item}/orders"

    # Send a POST request to create an order
    order_response = requests.get(order_url, headers=headers)

    data = order_response.text

    # Load the JSON data
    # Load the JSON data
    order_data = json.loads(data)

    # Initialize minimum and maximum values
    min_sell_platinum = float('inf')
    # max_buy_platinum = float('-inf')

    # Find the minimum and maximum platinum values for orders within the last 2 weeks
    two_weeks_ago = datetime.utcnow() - timedelta(days=1)

    for order in order_data['payload']['orders']:
        last_seen_date = datetime.fromisoformat(order['last_update'][:-6])  # Extract and parse the date
        if last_seen_date >= two_weeks_ago:
            if order['order_type'] == 'sell':
                min_sell_platinum = min(min_sell_platinum, order['platinum'])
            # elif order['order_type'] == 'buy':
            #     max_buy_platinum = max(max_buy_platinum, order['platinum'])

    # return int(max(min_sell_platinum, max_buy_platinum)*1.1)
    return int(min_sell_platinum*1.5)

# Function to remove a row from CSV file
def remove_item_from_csv(item_name):
    df = pd.read_csv('old_items.csv')
    df = df[df['Item Name'] != item_name]
    df.to_csv('old_items.csv', index=False)


def main():
    items = []
    listed_items = {}
    sum_plat = 0

    # Open and read the CSV file
    with open('old_items.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            items.append((row['Item Name'], int(row['Quantity'])))

    for item in items:
        try:
            if not is_listed(item[0]):
                try:
                    price = get_minimum_price(item[0])
                except:
                    logging.info(f"{item[0]} didn't work.")
                time.sleep(1)
                if int(price) >= TOO_LOW_PRICE:
                    try: 
                        list_item(item[0], price, item[1])
                        remove_item_from_csv(item[0])
                    except:
                        logging.info(f"{item[0]} didn't work.")
                    time.sleep(1)
                    listed_items[item[0]] = price
                    sum_plat += price * int(item[1])
                else:
                    logging.info(f"{item[0]} value is {price}, too low.")
            else:
                logging.info(f"{item[0]} is listed.")
                remove_item_from_csv(item[0])
                logging.info(f"Removed {item[0]} from file.")
        except:  # noqa: E722
            logging.info(f"Couldnt find out if [{item[0]}] is listed.")

    print(listed_items)
    print(f"Total Listed Value: {sum_plat}")

if __name__ == "__main__":
    main()
    
