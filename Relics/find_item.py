import csv
import sys

def find_item_price(target_item, csv_file: str = "item_prices.csv"):
    try:
        with open(csv_file, 'r', newline='') as file:
            reader = csv.DictReader(file)
            
            for row in reader:
                if row["Item"] == target_item:
                    return row["Price"]
            
            # If the target item is not found
            return f"Item '{target_item}' not found in the CSV file."
    except FileNotFoundError:
        return f"Error: File '{csv_file}' not found."
    except Exception as e:
        return f"An unexpected error occurred: {e}"