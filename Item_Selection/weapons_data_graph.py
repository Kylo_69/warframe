import pandas as pd
import os
from datetime import datetime
import matplotlib.pyplot as plt


def update_weapons_history():
    # Load the current data from the CSV
    df = pd.read_csv('warframe_data.csv')

    # Filter to get only warframes
    filtered_df = df[df['weapon type'] != 'warframe']

    # Sort the dataframe by usage
    sorted_df = filtered_df.sort_values(by='kills', ascending=False)

    # Get current timestamp
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # Load or create history CSV
    history_file = 'weapons_kills_history.csv'
    if os.path.exists(history_file):
        history_df = pd.read_csv(history_file)
    else:
        # Create an empty history DataFrame with only the Date column
        history_df = pd.DataFrame(columns=['Date'])

    # Ensure the Date column is in the DataFrame
    if 'Date' not in history_df.columns:
        history_df['Date'] = None

    # Prepare a new row with current usage data
    new_row = {'Date': current_time}
    for index, row in sorted_df.iterrows():
        new_row[row['item']] = row['kills']

    # Convert the new_row dictionary to a DataFrame
    new_row_df = pd.DataFrame([new_row])

    # Concatenate the new row with the existing history DataFrame
    history_df = pd.concat([history_df, new_row_df], ignore_index=True)

    # Fill NaN values with 0.0 for any missing Warframe data
    history_df = history_df.fillna(0.0)

    # Save the updated history to the CSV
    history_df.to_csv(history_file, index=False)
    
    with open(history_file, 'w') as f:
        history_df.to_csv(f, index=False)
        f.flush()


def update_weapons_usage_history():
    # Load the current data from the CSV
    df = pd.read_csv('warframe_data.csv')

    # Filter to get only warframes
    filtered_df = df[df['weapon type'] != 'warframe']

    # Sort the dataframe by usage
    sorted_df = filtered_df.sort_values(by='usage', ascending=False)

    # Get current timestamp
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # Load or create history CSV
    history_file = 'weapons_usage_history.csv'
    if os.path.exists(history_file):
        history_df = pd.read_csv(history_file)
    else:
        # Create an empty history DataFrame with only the Date column
        history_df = pd.DataFrame(columns=['Date'])

    # Ensure the Date column is in the DataFrame
    if 'Date' not in history_df.columns:
        history_df['Date'] = None

    # Prepare a new row with current usage data
    new_row = {'Date': current_time}
    for index, row in sorted_df.iterrows():
        new_row[row['item']] = row['usage']

    # Convert the new_row dictionary to a DataFrame
    new_row_df = pd.DataFrame([new_row])

    # Concatenate the new row with the existing history DataFrame
    history_df = pd.concat([history_df, new_row_df], ignore_index=True)

    # Fill NaN values with 0.0 for any missing Warframe data
    history_df = history_df.fillna(0.0)

    # Save the updated history to the CSV
    history_df.to_csv(history_file, index=False)
    
    with open(history_file, 'w') as f:
        history_df.to_csv(f, index=False)
        f.flush()



def plot_weapon_history(log, filename = "weapons_kills_history.csv"):
    # Load the historical data
    history_df = pd.read_csv(filename)

    # Convert the Date column to datetime for proper plotting
    history_df['Date'] = pd.to_datetime(history_df['Date'])

    # Set Date as index for easier plotting
    history_df.set_index('Date', inplace=True)

    # Calculate the total kills for each weapon to identify the top 10
    total_kills = history_df.max().sort_values(ascending=False)
    top_10_weapons = total_kills.head(10).index

    # Plot the data
    plt.figure(figsize=(19.2, 10.8), dpi=100)

    # Plot the top 10 weapons with distinctive colors
    for weapon in top_10_weapons:
        plt.plot(history_df.index, history_df[weapon], label=weapon)

    # Plot the remaining weapons with faded colors
    for weapon in history_df.columns:
        if weapon not in top_10_weapons:
            plt.plot(history_df.index, history_df[weapon], color='grey', alpha=0.3)

    
    if log:
        plt.yscale('log')  # Set y-axis to logarithmic scale
        plt.xlabel('Date')
        plt.ylabel('Kills')
        plt.title('Weapons Kills Over Time')
        plt.legend(loc='upper left', bbox_to_anchor=(1, 1))
        plt.xticks(rotation=45)
        plt.tight_layout()
    else:
        plt.xlabel('Date')
        plt.ylabel('Usage')
        plt.title('Weapons Usage Over Time')
        plt.legend(loc='upper left', bbox_to_anchor=(1, 1))
        plt.xticks(rotation=45)
        plt.tight_layout()

    # Save the plot
    plt.savefig(f'{filename[:-3]}png')

    # Show the plot
    #plt.show()
