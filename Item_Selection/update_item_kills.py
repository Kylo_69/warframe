import warframe_kills_v2
import sys
import csv


def load_data(filename):
    data = []
    with open(filename, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            data.append(row)
    return data

def save_data(filename, data):
    with open(filename, 'w', newline='') as file:
        fieldnames = ['item', 'weapon type_old', 'weapon type_new', 'kills']
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)


item = input("Input item: ").upper()
kills = input("Input kills: ")
print(f"Checking for {item}")

filename = 'warframe_data_new.csv'
data = load_data(filename)


item_data = [row for row in data if row['item'] == item]
save_data(filename, data)

warframe_kills_v2.update_item(item)