import csv
import random


def load_data(filename):
    data = []
    with open(filename, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            data.append(row)
    return data


def print_all_probabilities(data):
    # Extract items, usage counts, and weights from the data
    items = [item['item'] for item in data]
    usages = [float(item['usage']) for item in data]
    weights = [float(item['weight']) for item in data]

    # Calculate inverted usage values
    max_usage = max(usages) + 1  # To avoid division by zero
    inverted_usages = [max_usage - usage for usage in usages]

    # Apply weights to inverted usages
    weighted_inverted_usages = [inverted_usage * weight for inverted_usage, weight in zip(inverted_usages, weights)]

    # Calculate the total of weighted inverted usages
    total_weighted_inverted_usage = sum(weighted_inverted_usages)

    # Calculate probabilities based on weighted inverted usages
    probabilities = [weighted_inverted_usage / total_weighted_inverted_usage for weighted_inverted_usage in weighted_inverted_usages]

    all_items = {}

    # Add probabilities to the data
    for i, item in enumerate(data):
        all_items[item['item']] = float(round(probabilities[i],5))

    sorted_items = dict(sorted(all_items.items(), key=lambda item: item[1]))

    # Add probabilities to the data
    for item, probability in sorted_items.items():
        print(f"{item.ljust(30)}: {round(probability*100,3)}%")



filename = 'warframe_data.csv'
data = load_data(filename)
warframe_data = [row for row in data if row['weapon type'] == 'warframe']
print_all_probabilities(warframe_data)