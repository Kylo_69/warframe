import pandas as pd


def main(item1: str, item2: str, item3: str, item4: str, item5: str, item6: str):
    excluded_items = [item1.upper(), item2.upper(), item3.upper(), item4.upper(), item5.upper(), item6.upper()]
    # Read data from a text file
    # with open('data.txt', 'r') as file:
    #     data = file.readlines()

    # # Remove extra whitespace and split into items and values
    # lines = [line.strip().split(":") for line in data]

    # # Initialize an empty list to store cleaned lines
    # cleaned_lines = []

    # # Remove extra spaces and '<' and '>'
    # for line in data:
    #     item, value = line.strip().split(":")
    #     cleaned_item = item.strip("<>").strip()
    #     cleaned_item = cleaned_item.replace("  ", " ")
    #     cleaned_value = int(value.strip())
    #     cleaned_lines.append([cleaned_item, cleaned_value])

    # Create DataFrame from old data
    #old_df = pd.DataFrame(cleaned_lines, columns=['item', 'kills'])

    old_df = pd.read_csv('warframe_data_new.csv')
    # Order old DataFrame by kills descending
    old_df_sorted = old_df.sort_values(by='kills', ascending=False)

    # Get rank for items in old data
    old_df_sorted['old_rank'] = (old_df_sorted['kills'].rank(method='min', ascending=False)).astype(int)

    # Read new data from the second file
    new_df = pd.read_csv('warframe_data.csv')

    # Capitalize item names in the new data
    new_df['item'] = new_df['item'].str.upper()

    # Ignore entries with weapon_type 'warframe'
    # new_df = new_df[new_df['weapon type'] != 'warframe']
    try:
        columns_to_drop = ['weapon type_old', 'weapon type_new']
        old_df_sorted = old_df_sorted.drop(columns=columns_to_drop)
        columns_to_drop = ['probability_old', 'probability_new']
        old_df_sorted = old_df_sorted.drop(columns=columns_to_drop)
        columns_to_drop = ['weight_old', 'weight_new']
        old_df_sorted = old_df_sorted.drop(columns=columns_to_drop)
    except:
        print("🤷‍♀️")

    new_df = new_df.drop(columns='crit_or_status')
    new_df = new_df.drop(columns='yareli')
    new_df = new_df.drop(columns='koumei')
    new_df = new_df.drop(columns='mag')
    
    # Merge old and new data based on item names
    merged_df = pd.merge(old_df_sorted, new_df, on='item', how='outer', suffixes=('_old', '_new'))

    # Identify changes in kills
    merged_df['change_in_kills'] = merged_df['kills_new'] - merged_df['kills_old']

    # Update kills using new data
    merged_df['kills'] = merged_df['kills_new'].fillna(merged_df['kills_old']).astype(int)

    # Get rank for items in new data after merging
    merged_df['new_rank'] = merged_df['kills'].rank(method='min', ascending=False).astype(int)

    # Calculate change in rank
    merged_df['change_in_rank'] = merged_df['old_rank'] - merged_df['new_rank']
    temp_df = merged_df
    columns_to_drop = ['kills_old', 'old_rank', 'usage', 'change_in_kills', 'new_rank', 'change_in_rank', 'kills_new']
    temp_df = temp_df.drop(columns=columns_to_drop)
    temp_df.to_csv('warframe_data_new.csv', index=False)  # Set index=False if you don't want to write row indices

    merged_df = merged_df[merged_df['item'].isin(excluded_items)]

    # Display the differences
    print("Differences:")
    print(merged_df[['item', 'kills', 'old_rank', 'new_rank', 'change_in_rank', 'change_in_kills']].dropna().to_string())


def update_item(item1: str):
    excluded_items = [item1.upper()]
    # Read data from a text file
    # with open('data.txt', 'r') as file:
    #     data = file.readlines()

    # # Remove extra whitespace and split into items and values
    # lines = [line.strip().split(":") for line in data]

    # # Initialize an empty list to store cleaned lines
    # cleaned_lines = []

    # # Remove extra spaces and '<' and '>'
    # for line in data:
    #     item, value = line.strip().split(":")
    #     cleaned_item = item.strip("<>").strip()
    #     cleaned_item = cleaned_item.replace("  ", " ")
    #     cleaned_value = int(value.strip())
    #     cleaned_lines.append([cleaned_item, cleaned_value])

    # Create DataFrame from old data
    #old_df = pd.DataFrame(cleaned_lines, columns=['item', 'kills'])

    old_df = pd.read_csv('warframe_data_new.csv')
    # Order old DataFrame by kills descending
    old_df_sorted = old_df.sort_values(by='kills', ascending=False)

    # Get rank for items in old data
    old_df_sorted['old_rank'] = (old_df_sorted['kills'].rank(method='min', ascending=False)).astype(int)

    # Read new data from the second file
    new_df = pd.read_csv('warframe_data.csv')

    # Capitalize item names in the new data
    new_df['item'] = new_df['item'].str.upper()

    # Ignore entries with weapon_type 'warframe'
    # new_df = new_df[new_df['weapon type'] != 'warframe']
    try:
        columns_to_drop = ['weapon type_old', 'weapon type_new']
        old_df_sorted = old_df_sorted.drop(columns=columns_to_drop)
    except:
        print("🤷‍♀️")
    
    # Merge old and new data based on item names
    merged_df = pd.merge(old_df_sorted, new_df, on='item', how='outer', suffixes=('_old', '_new'))

    # Identify changes in kills
    merged_df['change_in_kills'] = merged_df['kills_new'] - merged_df['kills_old']

    # Update kills using new data
    merged_df['kills'] = merged_df['kills_new'].fillna(merged_df['kills_old']).astype(int)

    # Get rank for items in new data after merging
    merged_df['new_rank'] = merged_df['kills'].rank(method='min', ascending=False).astype(int)

    # Calculate change in rank
    merged_df['change_in_rank'] = merged_df['old_rank'] - merged_df['new_rank']
    temp_df = merged_df
    columns_to_drop = ['kills_old', 'old_rank', 'usage', 'change_in_kills', 'new_rank', 'change_in_rank', 'kills_new', 'weight', 'weight_old', 'yareli', 'mag', 'koumei']
    temp_df = temp_df.drop(columns=columns_to_drop)
    temp_df.to_csv('warframe_data_new.csv', index=False)  # Set index=False if you don't want to write row indices

    merged_df = merged_df[merged_df['item'].isin(excluded_items)]

    # Display the differences
    print("Differences:")
    print(merged_df[['item', 'kills', 'old_rank', 'new_rank', 'change_in_rank', 'change_in_kills']].dropna().to_string())


def all():
    
    #Read data from a text file
    with open('data.txt', 'r') as file:
        data = file.readlines()

    # Remove extra whitespace and split into items and values
    lines = [line.strip().split(":") for line in data]

    # Initialize an empty list to store cleaned lines
    cleaned_lines = []

    # Remove extra spaces and '<' and '>'
    for line in data:
        item, value = line.strip().split(":")
        cleaned_item = item.strip("<>").strip()
        cleaned_item = cleaned_item.replace("  ", " ")
        cleaned_value = int(value.strip())
        cleaned_lines.append([cleaned_item, cleaned_value])

    #Create DataFrame from old data
    old_df = pd.DataFrame(cleaned_lines, columns=['item', 'kills'])

    #old_df = pd.read_csv('warframe_data_new.csv')

    # Order old DataFrame by kills descending
    old_df_sorted = old_df.sort_values(by='kills', ascending=False)

    # Get rank for items in old data
    old_df_sorted['old_rank'] = (old_df_sorted['kills'].rank(method='min', ascending=False)).astype(int)

    # Read new data from the second file
    new_df = pd.read_csv('warframe_data.csv')

    # Capitalize item names in the new data
    new_df['item'] = new_df['item'].str.upper()

    # Ignore entries with weapon_type 'warframe'
    # new_df = new_df[new_df['weapon type'] != 'warframe']

    # Merge old and new data based on item names
    merged_df = pd.merge(old_df_sorted, new_df, on='item', how='outer', suffixes=('_old', '_new'))

    # Identify changes in kills
    merged_df['change_in_kills'] = merged_df['kills_new'] - merged_df['kills_old']

    # Update kills using new data
    merged_df['kills'] = merged_df['kills_new'].fillna(merged_df['kills_old']).astype(int)

    # Get rank for items in new data after merging
    merged_df['new_rank'] = merged_df['kills'].rank(method='min', ascending=False).astype(int)

    # Calculate change in rank
    merged_df['change_in_rank'] = merged_df['old_rank'] - merged_df['new_rank']

    # Display the differences
    print("Differences:")
    sort = input("Sort by? kills/change_in_kills\n")
    merged_df = merged_df.sort_values(by=sort, ascending=False)
    print(merged_df[['item', 'kills', 'old_rank', 'new_rank', 'change_in_rank', 'change_in_kills']].dropna().to_string())


if __name__ == "__main__":
    all()
    #main("Mag Prime", "Latron Prime", "Akstiletto Prime", "Ceti Lacera")