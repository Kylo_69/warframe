import random

def pick_warframe(data):
    # Extract items and their corresponding usage counts
    items = [item['item'] for item in data]
    usages = [float(item['usage']) for item in data]

    # Calculate inverted usage values
    max_usage = max(usages) + 1  # To avoid division by zero
    inverted_usages = [max_usage - usage for usage in usages]

    # Calculate the total of inverted usages
    total_inverted_usage = sum(inverted_usages)

    # Calculate probabilities based on inverted usages
    probabilities = [inverted_usage / total_inverted_usage for inverted_usage in inverted_usages]

    # Select a random item based on the calculated probabilities
    selected_item  = random.choices(items, weights=probabilities, k=1)[0]

    # max_length = 20
    # decimal_places = 3
    # # Header
    # print(f"{'Warframe':<{max_length}} {'Usage':<5} {'Probability'}")
    # print('-' * (max_length + 15))
    
    # # Rows
    # for row, prob in zip(data, probabilities):
    #     warframe_name = row['item'][:max_length]
    #     usage = row['usage']
    #     probability = round(prob, decimal_places)
    #     print(f"{warframe_name:<{max_length}} {usage:<5} {probability:.{decimal_places}f}")

    for i, item in enumerate(data):
        item['probability'] = probabilities[i]
        print(f"{item['probability']} - {item['item']}")

    # Retrieve the selected item's data including the probability
    chosen_row = next(item for item in data if item['item'] == selected_item)
    chosen_probability = chosen_row['probability']
    return chosen_row, chosen_probability


data = [
    {'item': 'Warframe1', 'usage': 30, 'weight': 0.25},
    {'item': 'Warframe2', 'usage': 40, 'weight': 0.5},
    {'item': 'Warframe3', 'usage': 50, 'weight': 1},
    {'item': 'Warframe4', 'usage': 60, 'weight': 2},
]

chosen_warframe, chosen_probability = pick_warframe(data)
print(f"Selected Warframe: {chosen_warframe['item']} with probability {chosen_probability:.3f}")
