import argparse
import csv
import os
import random
from datetime import datetime
import shutil
import sys

import pandas as pd
import warframe_kills_v2
import warframe_data_update_and_graph
import weapons_data_graph


def print_warframes():
    # Load the CSV file
    df = pd.read_csv('warframe_data.csv')

    # Filter the dataframe where weapon type is "warframe"
    filtered_df = df[df['weapon type'] == 'warframe']

    # Sort the dataframe by usage
    sorted_df = filtered_df.sort_values(by='usage', ascending=False)

    # Print the name (truncated to 20 characters) and usage
    for index, row in sorted_df.iterrows():
        name = (row['item'][:20]) if len(row['item']) > 20 else row['item']
        print(f"{name:20} {row['usage']}")


def load_data(filename):
    data = []
    with open(filename, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            data.append(row)
    return data


def time_to_minutes(time_str):
    """
    Converts a time string in the format "mm:ss" to a float representing the number of minutes.
    """
    minutes, seconds = map(int, time_str.split(':'))
    return round(minutes + seconds / 60,2)


def save_data(filename, data):
    with open(filename, 'w', newline='', buffering=1) as file:
        fieldnames = ['item', 'weapon type', 'kills', 'usage', 'crit_or_status', 'probability', 'weight', 'yareli', 'mag', 'koumei']
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)


def pick_weapon(data):
    # Extract items and their corresponding usage counts
    items = [item['item'] for item in data]
    kills = [float(item['usage']) for item in data]
    weights = [float(item['weight']) for item in data]


    # Calculate inverted usage values
    max_kills = max(kills) + 1000  # To avoid division by zero
    inverted_kills = [max_kills - usage for usage in kills]

    # Apply weights to inverted usages
    weighted_inverted_usages = [inverted_usage * weight for inverted_usage, weight in zip(inverted_kills, weights)]

    # Calculate the total of weighted inverted usages
    total_weighted_inverted_usage = sum(weighted_inverted_usages)

    # Calculate the total of inverted usages
    #total_inverted_kills = sum(inverted_kills)

    # Calculate probabilities based on weighted inverted usages
    probabilities = [weighted_inverted_usage / total_weighted_inverted_usage for weighted_inverted_usage in weighted_inverted_usages]

    # Calculate probabilities based on inverted usages
    #probabilities = [inverted_kills / total_inverted_kills for inverted_kills in inverted_kills]

    # Select a random item based on the calculated probabilities
    selected_item = random.choices(items, weights=probabilities, k=1)[0]

    # max_length = 20
    # decimal_places = 3
    # # Header
    # print(f"{'Item':<{max_length}} {'Kills':<5} {'Probability'}")
    # print('-' * (max_length + 15))
    
    # # Rows
    # for row, prob in zip(data, probabilities):
    #     warframe_name = row['item'][:max_length]
    #     usage = row['kills']
    #     probability = round(prob, decimal_places)
    #     print(f"{warframe_name:<{max_length}} {usage:<5} {probability:.{decimal_places}f}")
    
    for i, item in enumerate(data):
        item['probability'] = probabilities[i]

    # Retrieve the selected item's data including the probability
    chosen_row = next(item for item in data if item['item'] == selected_item)
    chosen_probability = chosen_row['probability']
    return chosen_row, chosen_probability


import random

def pick_warframe(data):
    # Extract items, usage counts, and weights from the data
    items = [item['item'] for item in data]
    usages = [float(item['usage']) for item in data]
    weights = [float(item['weight']) for item in data]

    # Calculate inverted usage values
    max_usage = max(usages) + 1  # To avoid division by zero
    inverted_usages = [max_usage - usage for usage in usages]

    # Apply weights to inverted usages
    weighted_inverted_usages = [inverted_usage * weight for inverted_usage, weight in zip(inverted_usages, weights)]

    # Calculate the total of weighted inverted usages
    total_weighted_inverted_usage = sum(weighted_inverted_usages)

    # Calculate probabilities based on weighted inverted usages
    probabilities = [weighted_inverted_usage / total_weighted_inverted_usage for weighted_inverted_usage in weighted_inverted_usages]

    # Select a random item based on the calculated probabilities
    selected_item  = random.choices(items, weights=probabilities, k=1)[0]

    # Add probabilities to the data
    for i, item in enumerate(data):
        item['probability'] = probabilities[i]

    # Retrieve the selected item's data including the probability
    chosen_row = next(item for item in data if item['item'] == selected_item)
    chosen_probability = chosen_row['probability']
    return chosen_row, chosen_probability


def log_updates(selected_warframe, selected_primary, selected_secondary, selected_melee, selected_companion):
    with open('update_log.txt', 'a') as log_file:
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        log_file.write(f"{timestamp}: Updated dataset\n")
        log_file.write(f"Warframe: {selected_warframe}\n")
        if selected_primary:
            log_file.write(f"Primary: {selected_primary}\n")
        if selected_secondary:
            log_file.write(f"Secondary: {selected_secondary}\n")
        if selected_melee:
            log_file.write(f"Melee: {selected_melee}\n")
        if selected_companion:
            log_file.write(f"Companion: {selected_companion}\n")
        log_file.write("\n")
        
        
def parse_arguments():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description="Item selection script for Warframe loadout.")
    
    parser.add_argument('--override', action='store_true', help='Override the default selection')
    parser.add_argument('--warframe', help='Select the Warframe')
    parser.add_argument('--primary', help='Select the primary weapon')
    parser.add_argument('--secondary', help='Select the secondary weapon')
    parser.add_argument('--melee', help='Select the melee weapon')

    args = parser.parse_args()

    return args


def main(): 
    filename = 'warframe_data.csv'
    backup_filename = "warframe_data_copy.csv"
    shutil.copy(filename, backup_filename)
    data = load_data(filename)
    data = sorted(data, key=lambda x: int(x['kills']), reverse=True)

    # Pick one item per weapon type based on kills
    primary_data = [row for row in data if row['weapon type'] == 'primary']
    secondary_data = [row for row in data if row['weapon type'] == 'secondary']
    melee_data = [row for row in data if row['weapon type'] == 'melee' and row['item'] != "Venka Prime"]
    warframe_data = [row for row in data if row['weapon type'] == 'warframe']
    companion_data = [row for row in data if row['weapon type'] == 'companion']
    companion_weapon_data = [row for row in data if row['weapon type'] == 'companion_weapon']

    # Check if --override is in sys.argv before parsing arguments
    if '--override' in sys.argv:
        args = parse_arguments()
    else:
        args = argparse.Namespace(override=False)  # Default to False if --override isn't specified

    if args.override:

        # Ensure warframe and at least one weapon is selected
        if not args.warframe:
            print("Error: A Warframe must be selected.")
            sys.exit(1)
        
        if not (args.primary or args.secondary or args.melee):
            print("Error: At least one weapon (primary, secondary, or melee) must be selected.")
            sys.exit(1)

        # Output the selected items
        selected_warframe, warframe_probability = pick_warframe([row for row in warframe_data if row['item'] in [args.warframe]])
        print(selected_warframe)
        if args.primary:
            print(args.primary)
            selected_primary, primary_probability = pick_weapon([row for row in primary_data if row['item'] in [args.primary]])
        else:
            selected_primary = None
        if args.secondary:
            selected_secondary, secondary_probability = pick_weapon([row for row in secondary_data if row['item'] in [args.secondary]])
        else:
            selected_secondary = None
        if args.melee:
            selected_melee, melee_probability = pick_weapon([row for row in melee_data if row['item'] in [args.melee]])
        else:
            selected_melee = None
        
        selected_companion, companion_probability = pick_weapon(companion_data)
        selected_companion_weapon = None
    
    else:


        if len(sys.argv) > 1:
            warframe_data = [row for row in data if row['weapon type'] == 'warframe']

            arguments = sys.argv[1:]

            # Select a random argument
            input_warframe = random.choice(arguments)

            try:
                selected_warframe, warframe_probability = pick_warframe([row for row in warframe_data if row['item'] in arguments])
                #selected_warframe = [row for row in data if row['weapon type'] == 'warframe' and row['item'] == input_warframe][0]
                #warframe_probability = 1 / (len(sys.argv)-1)

            except Exception as e:
                print(e)
                print_warframes()
                return
            
            
            companion_data = [row for row in companion_data if row['crit_or_status'] != 'mecha']
            selected_companion, companion_probability = pick_weapon(companion_data)
            
        else:
            warframe_data = [row for row in data if row['weapon type'] == 'warframe']
            selected_warframe, warframe_probability = pick_warframe(warframe_data)



        if selected_warframe['item'] != "Inaros Prime":
            companion_data = [row for row in companion_data if row['crit_or_status'] != 'strain']

        # Pick one item per weapon type based on kills
        if selected_warframe['item'] != 'Mag Prime':
            primary_data = [row for row in data if row['weapon type'] == 'primary' and row['crit_or_status'] != "mag"]
            secondary_data = [row for row in data if row['weapon type'] == 'secondary' and row['crit_or_status'] != "mag"]
            melee_data = [row for row in data if row['weapon type'] == 'melee' and row['crit_or_status'] != "mag"]
            

        # Pick one item per weapon type based on kills
        if selected_warframe['item'] == 'Nyx Prime':
            if random.random() < 0.5:
                print("### MECHA ###")
                companion_data = [row for row in data if row['weapon type'] == 'companion']
                companion_data = [row for row in companion_data if row['crit_or_status'] == 'mecha']
            selected_companion, companion_probability = pick_weapon(companion_data)
            selected_primary, primary_probability = pick_weapon(primary_data)
            selected_secondary, secondary_probability = pick_weapon(secondary_data)
            selected_melee = None

        # Only select a melee weapon for Kullervo
        elif selected_warframe['item'] == 'Kullervo':
            print("##########")
            print("Select Naramon Focus School")
            print("##########")
            print("Select Akstiletto Prime & Zenith Prime Builds C with the Dexterity Arcane.")
            selected_melee, melee_probability = pick_weapon(melee_data)
            selected_primary = None
            selected_secondary = None

        # Only select a melee weapon for Valkyr or Hysteria
        elif selected_warframe['item'] == 'Valkyr Prime':
            print("##########")
            print("Select Naramon Focus School")
            print("##########")
            print("Select Akstiletto Prime & Zenith Prime Builds C with the Dexterity Arcane.")
            if random.random() < 0.5:
                selected_melee, melee_probability = pick_weapon(melee_data)
            else:
                selected_melee = None

            selected_primary = None
            selected_secondary = None
            
        elif selected_warframe['item'] == 'Voruna':
            print("##########")
            print("Select Naramon Focus School")
            print("##########")
            print("Select Akstiletto Prime & Zenith Prime Builds C with the Dexterity Arcane.")
            selected_melee, melee_probability = pick_weapon(melee_data)

            selected_primary = None
            selected_secondary = None

        elif selected_warframe['item'] == 'Wukong Prime':
            if random.random() < 0.6:
                random_number = random.random()
                if  random_number < 0.33:
                    selected_primary, primary_probability = pick_weapon(primary_data)
                    selected_secondary, secondary_probability = pick_weapon(secondary_data)
                    selected_melee = None
                elif  random_number < 0.66:
                    selected_primary, primary_probability = pick_weapon(primary_data)
                    selected_secondary = None
                    selected_melee, melee_probability = pick_weapon(melee_data)
                else:
                    selected_primary = None
                    selected_secondary, secondary_probability = pick_weapon(secondary_data)
                    selected_melee, melee_probability = pick_weapon(melee_data)
                
            else:
                selected_primary = None
                selected_secondary = None
                selected_melee = None 
                        
                print("##########")
                print("Select Naramon Focus School")
                print("##########")
                print("Select Akstiletto Prime & Zenith Prime Builds C with the Dexterity Arcane.")

        elif selected_warframe['item'] == 'Baruuk Prime':
            print("##########")
            print("Select Naramon Focus School")
            print("##########")
            print("Select Akstiletto Prime & Zenith Prime Builds C with the Dexterity Arcane.")
            # Baruuk any weapon goes + Serene Storm
            selected_melee = None
            selected_primary = None
            selected_secondary = None

        elif selected_warframe['item'] == 'Equinox Prime':
            print("##########")
            print("Make sure to select the Sharpshooter Sniper Build.")
            print("##########")
            # Equinox needs Sharpshooter Sniper
            selected_primary, primary_probability = pick_weapon([row for row in primary_data if row['item'] in ['Vectis Prime', 'Vulkar Wraith']])
            selected_secondary, secondary_probability = pick_weapon(secondary_data)
            selected_melee = None

        elif selected_warframe['item'] == 'Ivara Prime':
        # Ivara only bow + Melee, or Artemis Bow
            if random.random() < 0.5:
                selected_primary, primary_probability = pick_weapon([row for row in primary_data if row['item'] in ['Rakta Cernos', 'Dread', 'Daikyu']])
                selected_secondary = None
                selected_melee = None
            else:
                selected_primary = None
                selected_secondary = None
                selected_melee = None

        elif selected_warframe['item'] == 'Titania Prime':
            # No weapon needed
            selected_melee = None
            selected_primary = None
            selected_secondary = None

        elif selected_warframe['item'] == 'Mesa Prime' or  selected_warframe['item'] == 'Mirage Prime' or selected_warframe['item'] == "Zephyr Prime":
            # No weapon needed
            selected_primary, primary_probability = pick_weapon(primary_data)
            selected_secondary, secondary_probability = pick_weapon(secondary_data)
            selected_melee = None
            print("##########")
            print("Equip Furax Wraith for the secondary fire rate bonus.")
            print("##########")

        elif selected_warframe['item'] == 'Yareli':
            selected_primary = None
            selected_secondary, secondary_probability = pick_weapon([row for row in secondary_data if row['yareli'] == "True"])
            selected_melee = None
                        
        elif selected_warframe['item'] == 'Xaku Prime':
            random_number = random.random()
            if random_number < 0.33:
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary = None
                selected_melee = None
            elif random_number < 0.66:
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary, secondary_probability = pick_weapon(secondary_data)
                selected_melee = None
            else:
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary = None
                selected_melee, melee_probability = pick_weapon(melee_data)

        elif selected_warframe['item'] == 'Ember Prime':
            if random.random() > 0.5:
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary = None
            else:
                selected_secondary, secondary_probability = pick_weapon(secondary_data)
                selected_primary = None
        
            selected_melee = None
                        
        elif selected_warframe['item'] == 'Styanax':
            if random.random() > 0.5:
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary = None
            else:
                selected_secondary, secondary_probability = pick_weapon(secondary_data)
                selected_primary = None
        
            selected_melee = None
                
        elif selected_warframe['item'] == 'Mag Prime':
            selected_primary, primary_probability = pick_weapon([row for row in primary_data if row['mag'] == "True"])
            selected_secondary, secondary_probability = pick_weapon([row for row in secondary_data if row['mag'] == "True"])
            selected_melee = None

        elif selected_warframe['item'] == 'Koumei':
            selected_primary, primary_probability = pick_weapon([row for row in primary_data if row['koumei'] == "True"])
            selected_secondary, secondary_probability = pick_weapon([row for row in secondary_data if row['koumei'] == "True"])
            selected_melee, melee_probability = pick_weapon([row for row in melee_data if row['koumei'] == "True"])

        elif selected_warframe['item'] == 'Caliban':
            if random.random() < 0.5:
                selected_primary, primary_probability = pick_weapon([row for row in primary_data if row['crit_or_status'] in ['hybrid', 'status']])
                selected_secondary = None            
                selected_melee = None
            else:
                selected_primary = None
                selected_secondary, secondary_probability = pick_weapon([row for row in secondary_data if row['crit_or_status'] in ['hybrid', 'status']])
                selected_melee = None

        elif selected_warframe['item'] == 'Garuda Prime':
            if random.random() < 0.5:
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary, secondary_probability = pick_weapon(secondary_data)
                selected_melee = None
            else:
                print("##########")
                print("Select Naramon Focus School")
                print("##########")
                print("Select Akstiletto Prime & Zenith Prime Builds C with the Dexterity Arcane.")
                melee_data = [row for row in data if row['item'] == "Venka Prime"]
                selected_melee, melee_probability = pick_weapon(melee_data)
                selected_primary = None
                selected_secondary = None
                
        elif selected_warframe['item'] == 'Cyte-09':
            if random.choice(["Sniper Rifle", "Gun Platform"]) == "Sniper Rifle":
                # Cyte-09 using his sniper rifle
                selected_primary = None
                selected_secondary = None
                selected_melee = None
            else:
                # Cyte-09 using his gun platform
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary, secondary_probability = pick_weapon(secondary_data)
                selected_melee = None               
                
                
        else:

            if selected_warframe['item'] == 'Gara Prime':
                print("Put Melee Crescendo on your melee weapon.")


            ### MECHA ###
            if selected_warframe['item'] == 'Inaros Prime':
                if random.random() > 0.5:
                    companion_data = [row for row in data if row['weapon type'] == 'companion']
                    companion_data = [row for row in companion_data if row['crit_or_status'] == 'mecha']
                else:
                    companion_data = [row for row in companion_data if row['crit_or_status'] == 'strain']
                
                selected_companion, companion_probability = pick_weapon(companion_data)

            if  selected_warframe['item'] == 'Frost Prime':
                companion_data = [row for row in data if row['weapon type'] == 'companion']
                companion_data = [row for row in companion_data if row['crit_or_status'] == 'mecha']
                selected_companion, companion_probability = pick_weapon(companion_data)

            if  selected_warframe['item'] == 'Harrow Prime':
                if random.random() > 0.5:
                    companion_data = [row for row in data if row['weapon type'] == 'companion']
                    companion_data = [row for row in companion_data if row['crit_or_status'] == 'mecha']
                selected_companion, companion_probability = pick_weapon(companion_data)

            if  selected_warframe['item'] == 'Koumei' or selected_warframe['item'] == 'Voruna' or selected_warframe['item'] == 'Banshee Prime' or selected_warframe['item'] == 'Rhino Prime':
                companion_data = [row for row in data if row['weapon type'] == 'companion']
                companion_data = [row for row in companion_data if row['crit_or_status'] == 'mecha']
                selected_companion, companion_probability = pick_weapon(companion_data)

            # For other warframes
            random_number = random.random()
            if random_number < 0.33:
                selected_primary, primary_probability = pick_weapon(primary_data)
                selected_secondary = None
                selected_melee = None
            elif random_number < 0.66:
                selected_primary = None
                selected_secondary, secondary_probability = pick_weapon(secondary_data)
                selected_melee = None
            else:
                selected_primary = None
                selected_secondary = None
                selected_melee, melee_probability = pick_weapon(melee_data)
    

    if selected_companion['crit_or_status'] == 'robotic':
        selected_companion_weapon, selected_companion_weapon_probability = pick_weapon(companion_weapon_data)
    else:
        selected_companion_weapon = None


    # Print selected items
    print(f"------> {selected_warframe['item']} — Probability: {round(warframe_probability*100,3)}%")
    #print(selected_warframe)
    if selected_primary:
        print(f"Selected Primary: {selected_primary['item']} — Probability: {round(primary_probability*100,3)}%")
    if selected_secondary:
        print(f"Selected Secondary: {selected_secondary['item']} — Probability: {round(secondary_probability*100,3)}%")
        if not selected_melee:
            print("##########")
            print("Equip Furax Wraith for the secondary fire rate bonus.")
            print("##########")
    if selected_melee:
        print(f"Selected Melee: {selected_melee['item']} — Probability: {round(melee_probability*100,3)}%")
        if not selected_secondary and not selected_primary:
            print("##########")
            print("Select Naramon Focus School")
            print("##########")
            print("Select Akstiletto Prime & Zenith Prime Builds C with the Dexterity Arcane.")
    if selected_companion:
        print(f"Selected Companion: {selected_companion['item']} - Probability: {round(companion_probability*100,3)}%")
    if selected_companion_weapon:
        print(f"Selected Companion: {selected_companion_weapon['item']} - Probability: {round(selected_companion_weapon_probability*100,3)}%")



    # Update kills for selected items
    duration = input("How long was the mission?")
    duration = time_to_minutes(duration)
    print(f"Duration of the Mission was: {duration}")
    
    selected_warframe['usage'] = float(selected_warframe['usage']) + duration
    if selected_melee:
        selected_melee['usage'] = float(selected_melee['usage']) + duration
    if selected_primary:
        selected_primary['usage'] = float(selected_primary['usage']) + duration
    if selected_secondary:
        selected_secondary['usage'] = float(selected_secondary['usage']) + duration  
    if selected_companion:
        selected_companion['usage'] = float(selected_companion['usage']) + duration  
    
    log_updates(selected_warframe, selected_primary, selected_secondary, selected_melee, selected_companion)
    warframe_data_update_and_graph.update_warframe_history()
    weapons_data_graph.update_weapons_usage_history()


    import get_kills 
    
    get_kills.get_weapon_stats()

    selected_warframe['kills'] = get_kills.get_weapon_kills(selected_warframe['item'])
    
    if selected_primary:
        selected_primary['kills'] = get_kills.get_weapon_kills(selected_primary['item'])
    
    if selected_secondary:
        selected_secondary['kills'] = get_kills.get_weapon_kills(selected_secondary['item'])
    
    if selected_melee:
        selected_melee['kills'] = get_kills.get_weapon_kills(selected_melee['item'])
    
    if selected_companion:
        selected_companion['kills']  = get_kills.get_weapon_kills(selected_companion['item'])
    
    if selected_companion_weapon:
        selected_companion_weapon['kills']  = get_kills.get_weapon_kills(selected_companion_weapon['item'])



    # if duration <= 30.0:
    #     save_data(filename, data)
    
    # if duration > 30.0:
    #     print("https://browse.wf/profile#account=573482453ade7f46ceb95930&platform=pc&tab=stats")
            
    #     temp = input(f"Previously: {selected_warframe['kills']}\nEnter new kills for {selected_warframe['item']}:")
    #     if temp == "":
    #         selected_warframe['kills'] = selected_warframe['kills']
    #     else:
    #         selected_warframe['kills'] = temp

    #     if selected_primary:
    #         temp = input(f"Previously: {selected_primary['kills']}\nEnter new kills for {selected_primary['item']}:")
    #         if temp == "":
    #             selected_primary['kills'] = selected_primary['kills']
    #         else:
    #             selected_primary['kills'] = temp

    #     if selected_secondary:
    #         temp = input(f"Previously: {selected_secondary['kills']}\nEnter new kills for {selected_secondary['item']}:")
    #         if temp == "":
    #             selected_secondary['kills'] = selected_secondary['kills']
    #         else:
    #             selected_secondary['kills'] = temp
        
    #     if selected_melee:
    #         temp = input(f"Previously: {selected_melee['kills']}\nEnter new kills for {selected_melee['item']}:")
    #         if temp == "":
    #             selected_melee['kills'] = selected_melee['kills']
    #         else:
    #             selected_melee['kills'] = temp
        
    #     if selected_companion:
    #         temp = input(f"Previously: {selected_companion['kills']}\nEnter new kills for {selected_companion['item']}:")
    #         if temp == "":
    #             selected_companion['kills'] = selected_companion['kills']
    #         else:
    #             selected_companion['kills'] = temp
        
    #     if selected_companion_weapon:
    #         temp = input(f"Previously: {selected_companion_weapon['kills']}\nEnter new kills for {selected_companion_weapon['item']}:")
    #         if temp == "":
    #             selected_companion_weapon['kills'] = selected_companion_weapon['kills']
    #         else:
    #             selected_companion_weapon['kills'] = temp

    # Update the CSV file
    save_data(filename, data)
    weapons_data_graph.update_weapons_history()
    warframe_data_update_and_graph.update_warframe_kills_history()
    warframe = selected_warframe['item'] if selected_warframe else ""
    print_warframes()
    primary = selected_primary['item'] if selected_primary else ""
    secondary = selected_secondary['item'] if selected_secondary else ""
    melee = selected_melee['item'] if selected_melee else ""
    companion = selected_companion['item'] if selected_companion else ""
    companion_weapon = selected_companion_weapon['item'] if selected_companion_weapon else ""
    warframe_kills_v2.main(warframe, primary, secondary, melee, companion, companion_weapon)
    warframe_data_update_and_graph.plot_warframe_history(False)
    warframe_data_update_and_graph.plot_warframe_history(True, "warframe_kills_history.csv")
    weapons_data_graph.plot_weapon_history(True) 
    weapons_data_graph.plot_weapon_history(False, 'weapons_usage_history.csv')

    os.remove(backup_filename)


if __name__ == "__main__":
    main()
