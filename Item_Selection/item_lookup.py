import requests

class WarframeItemLookup:
    def __init__(self):
        self._items = None
        self._items_dict = None  # For faster lookup by uniqueName
    
    def _fetch_data(self):
        """Load data from GitHub if not already cached"""
        if self._items is None:
            try:
                response = requests.get(
                    "https://raw.githubusercontent.com/WFCD/warframe-items/master/data/json/All.json"
                )
                response.raise_for_status()
                self._items = response.json()
                # Create a dictionary for O(1) lookups
                self._items_dict = {item["uniqueName"].lower(): item 
                                  for item in self._items if "uniqueName" in item}
            except Exception as e:
                raise RuntimeError(f"Failed to load item data: {str(e)}")

    def find_name(self, internal_path):
        """Find item name by its internal path"""
        self._fetch_data()  # Ensure data is loaded
        
        # Normalize the path for case-insensitive comparison
        normalized_path = internal_path.strip().lower()
        return self._items_dict.get(normalized_path, {}).get("name", "Unknown Item")

# # Initialize once at startup
# item_lookup = WarframeItemLookup()

# # Example usage - initial call will fetch data
# print(item_lookup.find_name(
#     "/Lotus/Weapons/Corpus/Pistols/CrpSentExperimentPistol/CrpSentExperimentPistol"
# ))  # Output: Ocucor

# # Subsequent calls use cached data
# print(item_lookup.find_name("/Lotus/Weapons/Tenno/LongGuns/DexSybaris"))  # Output: Dex Sybaris
# print(item_lookup.find_name("/Lotus/Weapons/Grineer/Melee/GrnHeavyHammer"))  # Output: Fragor