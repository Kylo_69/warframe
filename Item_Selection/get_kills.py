from datetime import datetime, timezone
import os
import requests
import csv
import json
from dotenv import load_dotenv

import item_lookup


# Initialize once at startup
Get_Item = item_lookup.WarframeItemLookup()



def get_weapon_stats(User_ID: str = "573482453ade7f46ceb95930"):
    url = "https://conduit.browse.wf/profilebyid"
    params = {
        "account": User_ID,
        "platform": "pc"
    }
    
    try:
        response = requests.get(url, params=params)
        response.raise_for_status()
        data = response.json()
        
        weapons = data.get('Stats', {}).get('Weapons', [])
        username = data["Results"][0]["DisplayName"]
        if "." in username:
            username =  username.replace(".", "")
        time_now = datetime.now(timezone.utc).isoformat().replace(":", "-").replace("T", "_")  # Replacing T for better readability

        if not weapons:
            print("No weapons data found")
            return

        weapon_list = []
        #print(f"Found {len(weapons)} weapons:")
        #print("-" * 50)
        
        for index, weapon in enumerate(weapons, 1):
            type_path = weapon.get('type', '')
            weapon_name = type_path.strip() if type_path else 'Unknown'

            stats = {
                'Internal Name': weapon_name,
                'Weapon': Get_Item.find_name(weapon_name),
                'Fired': weapon.get('fired', 0),
                'Hits': weapon.get('hits', 0),
                'Kills': weapon.get('kills', 0),
                'Headshots': weapon.get('headshots', 0),
                'Assists': weapon.get('assists', 0),
                'XP': weapon.get('xp', 0),
                'Equip Time': weapon.get('equipTime', 0),
                'Equip Time (readable)': format_seconds(weapon.get('equipTime', 0))
            }
            weapon_list.append(stats)
            
            # Optional console output
            # print(f"Weapon {index}: {stats['Weapon']}")
            # for key, value in stats.items():
            #     if key == 'Weapon':
            #         continue
            #     print(f"{key:>10}: {value}")
            # print("-" * 50)

        # Write to CSV
        csv_filename = f'stats.csv'
        with open(csv_filename, 'w', newline='') as csvfile:
            fieldnames = ['Internal Name', 'Weapon', 'Fired', 'Hits', 'Kills', 'Headshots', 
                         'Assists', 'XP', 'Equip Time', 'Equip Time (readable)']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(weapon_list)
            
        #print(f"\nSuccessfully saved {len(weapon_list)} weapons to {csv_filename}")

    except requests.exceptions.RequestException as e:
        print(f"Request failed: {e}")
    except json.JSONDecodeError:
        print("Failed to parse JSON response")
    except Exception as e:
        print(f"Error occurred: {e}")


def format_seconds(total_seconds: float) -> str:
    """
    Convert seconds to hours, minutes, and remaining seconds.
    Preserves decimal precision for the seconds component.
    
    Example: 6877584.046871649 → "1910 hours 26 minutes 24.046871649 seconds"
    """
    hours = int(total_seconds // 3600)
    remainder = total_seconds % 3600
    minutes = int(remainder // 60)
    seconds = remainder % 60
    
    return f"{hours} hours {minutes} minutes {seconds:.9f} seconds".rstrip('0').rstrip('.')


def get_weapon_kills(target_weapon, csv_path = 'stats.csv'):
    total_kills = 0
    with open(csv_path, mode='r', newline='') as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=[
            'Internal Name', 'Weapon', 'Fired', 'Hits', 'Kills', 
            'Headshots', 'Assists', 'XP', 'Equip Time', 'Equip Time (readable)'
        ])
        
        # Skip header if your CSV already has one
        next(reader)
        
        for row in reader:
            if row['Weapon'] == target_weapon:
                try:
                    total_kills += int(row['Kills'])
                except (ValueError, KeyError):
                    # Handle missing/invalid data
                    pass
    return total_kills
